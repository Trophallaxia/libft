/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_merge.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/23 15:13:56 by sabonifa          #+#    #+#             */
/*   Updated: 2018/07/24 18:38:59 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	ft_list_merge(t_list **begin_list1, t_list *begin_list2)
{
	t_list	*curr_elem;

	if (begin_list1 == NULL)
		return ;
	if (*begin_list1 == NULL)
		*begin_list1 = begin_list2;
	else
	{
		curr_elem = *begin_list1;
		while (curr_elem->next != NULL)
			curr_elem = curr_elem->next;
		curr_elem->next = begin_list2;
	}
}
