/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/11 14:20:30 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/14 18:47:16 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new_lst;
	t_list	*curr_elem;
	t_list	*new_elem;
	t_list	*tp_lst;

	if (!lst || !f)
		return (NULL);
	tp_lst = lst;
	new_elem = NULL;
	if (!(new_lst = (t_list*)malloc(sizeof(*new_lst) * 1)))
		return (NULL);
	new_lst = ft_lstnew(f(tp_lst)->content, f(tp_lst)->content_size);
	curr_elem = new_lst;
	curr_elem->next = new_elem;
	tp_lst = tp_lst->next;
	while (tp_lst)
	{
		new_elem = ft_lstnew(f(tp_lst)->content, f(tp_lst)->content_size);
		curr_elem->next = new_elem;
		curr_elem = curr_elem->next;
		tp_lst = tp_lst->next;
	}
	curr_elem->next = NULL;
	return (new_lst);
}
