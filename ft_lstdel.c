/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/11 13:09:06 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/13 18:43:25 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*curr_elem;

	while (*alst != NULL)
	{
		curr_elem = *alst;
		ft_lstdelone(&curr_elem, del);
		*alst = (**alst).next;
	}
	*alst = NULL;
}
