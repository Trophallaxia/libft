/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/09 16:20:51 by sabonifa          #+#    #+#             */
/*   Updated: 2018/07/17 17:14:59 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char	*ft_strcapitalize(char *str)
{
	size_t	i;

	i = 0;
	ft_strlowcase(str);
	if ('a' <= str[i] && str[i] <= 'z')
	{
		str[i] = str[i] - 32;
		i++;
	}
	while (str[i])
	{
		if (('a' <= str[i] && str[i] <= 'z') &&
				!(('a' <= str[i - 1] && str[i - 1] <= 'z') ||
					('0' <= str[i - 1] && str[i - 1] <= '9') ||
						('A' <= str[i - 1] && str[i - 1] <= 'Z')))
			str[i] = str[i] - 32;
		i++;
	}
	return (str);
}
