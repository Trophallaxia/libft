/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 14:02:34 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/14 15:04:37 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char			*ft_strmap(char const *s, char (*f)(char))
{
	unsigned int	s_len;
	unsigned int	i;
	char			*dup;

	i = 0;
	if (!s || !f)
		return (NULL);
	s_len = ft_strlen((char*)s);
	if (!(dup = (char*)malloc(sizeof(*dup) * (s_len + 1))))
		return (NULL);
	while (i < s_len)
	{
		dup[i] = f(s[i]);
		i++;
	}
	dup[i] = 0;
	return (dup);
}
